package com.js.personapi.dao;

import org.springframework.data.repository.CrudRepository;

import com.js.personapi.model.Person;

public interface PersonRepository extends CrudRepository<Person, Integer> {

}
