package com.js.personapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan(basePackages="com.js.personapi")
public class PersonApplication {

	public static void main(String[] args) {
		
		SpringApplication.run(PersonApplication.class, args);

	}

}
