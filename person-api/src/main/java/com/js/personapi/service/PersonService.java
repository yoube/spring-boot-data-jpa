package com.js.personapi.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.js.personapi.dao.PersonRepository;
import com.js.personapi.model.Person;

@Service
public class PersonService {

    @Autowired
    PersonRepository personRepository;

    public List<Person> getAllPersons() {
        List<Person> persons = new ArrayList<>();
        personRepository.findAll().forEach(person -> persons.add(person));
        return persons;
    }

    public Person getPersonById(final int id) {
        return personRepository.findById(id).get();
    }

    public void save(final Person person) {
        personRepository.save(person);
    }
    
    public void update(final Person person) {
        personRepository.save(person);
    }

    public void delete(final int id) {
        personRepository.deleteById(id);
    }
}