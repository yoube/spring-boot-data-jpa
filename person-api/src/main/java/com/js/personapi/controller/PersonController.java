package com.js.personapi.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.js.personapi.model.Person;
import com.js.personapi.service.PersonService;

@RestController
public class PersonController {
	
	@Autowired
    private PersonService personService;

    @GetMapping("/persons")
    private List<Person> getAllPersons() {
        return personService.getAllPersons();
    }
    
    @PostMapping("/persons")
    private int savePerson(@RequestBody final Person person) {
    	personService.save(person);
    	return person.getId();
    }

    @GetMapping("/persons/{id}")
    private Person getPerson(@PathVariable("id") final int id) {
        return personService.getPersonById(id);
    }

    @DeleteMapping("/persons/{id}")
    private void deletePerson(@PathVariable("id") final int id) {
        personService.delete(id);
    }

    
    @PutMapping("/persons")
    private int updatePerson(@RequestBody final Person person) {
        personService.update(person);
        return person.getId();
    }
}
